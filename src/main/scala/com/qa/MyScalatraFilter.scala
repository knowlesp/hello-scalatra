package com.qa

import org.scalatra._
import java.net.URL
import scalate.ScalateSupport

class MyScalatraFilter extends ScalatraFilter with ScalateSupport {
  
  get("/") {
    <html>
      <body>
        <h1>Hello, world!</h1>
        What's that about then? Saying hello to Scalatra from Maven and Jenkins with the new things and with services!
		<h2>Hello DWP!</h2>
		<p>How are you doing today?</p>
      </body>
    </html>
  }

  notFound {
    // If no route matches, then try to render a Scaml template
    val templateBase = requestPath match {
      case s if s.endsWith("/") => s + "index"
      case s => s
    }
    val templatePath = "/WEB-INF/scalate/templates/" + templateBase + ".scaml"
    servletContext.getResource(templatePath) match {
      case url: URL => 
        contentType = "text/html"
        templateEngine.layout(templatePath)
      case _ => 
        filterChain.doFilter(request, response)
    } 
  }
}
